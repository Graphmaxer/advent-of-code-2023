import { createReadStream } from 'node:fs';
import { createInterface } from 'node:readline';

export function createReadlineInterface(path: string) {
  const fileStream = createReadStream(path);
  return createInterface({ input: fileStream });
}
