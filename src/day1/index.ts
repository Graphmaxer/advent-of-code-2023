import { createReadlineInterface } from '../utils.js';

const NUMBERS = ['one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine'];

export const isCharNumber = (char: string): boolean => char.length === 1 && char >= '0' && char <= '9';
export const concatNumbers = (numbers: number[]): number => Number(`${numbers.at(0)}${numbers.at(-1)}`);

export function parseNumberAtIndex(value: string, idx: number): number | null {
  for (const [i, number] of NUMBERS.entries()) {
    if (value.slice(idx).startsWith(number)) {
      return i + 1;
    }
  }
  return null;
}

export function parseNumbers(value: string, withStringNumbers = true): number[] {
  const numbers: number[] = [];

  for (let i = 0; i < value.length; i++) {
    if (isCharNumber(value[i])) {
      numbers.push(Number(value[i]));
    } else if (withStringNumbers) {
      const parsed = parseNumberAtIndex(value, i);
      if (parsed) {
        numbers.push(parsed);
      }
    }
  }

  return numbers;
}

export async function runPartOne(path: string) {
  const rl = createReadlineInterface(path);
  const lines: number[] = [];

  for await (const line of rl) {
    const numbers = parseNumbers(line, false);
    if (numbers.length) {
      lines.push(concatNumbers(numbers));
    }
  }

  const sum = lines.reduce((acc: number, value: number) => acc + value, 0);

  return sum;
}

export async function runPartTwo(path: string) {
  const rl = createReadlineInterface(path);
  const lines: number[] = [];

  for await (const line of rl) {
    const numbers = parseNumbers(line);
    if (numbers.length) {
      lines.push(concatNumbers(numbers));
    }
  }

  const sum = lines.reduce((acc: number, value: number) => acc + value, 0);

  return sum;
}
