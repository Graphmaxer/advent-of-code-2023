import assert from 'node:assert';
import { describe, it } from 'node:test';
import { concatNumbers, isCharNumber, parseNumberAtIndex, parseNumbers } from './index.js';

void describe('Day 1', () => {
  void describe('isCharNumber', () => {
    const tests: [string, boolean][] = [
      ['0', true],
      ['1', true],
      ['2', true],
      ['3', true],
      ['4', true],
      ['5', true],
      ['6', true],
      ['7', true],
      ['8', true],
      ['9', true],
      ['10', false],
      ['-1', false],
      ['a', false],
      ['z', false],
      ['A', false],
      ['Z', false],
      ['0a', false],
    ];

    for (const test of tests) {
      void it(`should ${test[1] ? 'detect' : 'not detect'} ${test[0]} as number`, () => {
        assert.strictEqual(isCharNumber(test[0]), test[1]);
      });
    }
  });

  void describe('parseNumberAtIndex', () => {
    const tests: [string, number, number | null][] = [
      ['two1nine', 0, 2],
      ['two1nine', 1, null],
      ['two1nine', 2, null],
      ['two1nine', 3, null],
      ['two1nine', 4, 9],
      ['two1nine', 5, null],
      ['two1nine', 6, null],
      ['two1nine', 7, null],
      ['eightwothree', 0, 8],
      ['eightwothree', 1, null],
      ['eightwothree', 2, null],
      ['eightwothree', 3, null],
      ['eightwothree', 4, 2],
      ['eightwothree', 5, null],
    ];

    for (const test of tests) {
      void it(`should ${test[2] ? 'parse' : 'not parse'} number at position ${test[1]} in string '${test[0]}'`, () => {
        assert.deepStrictEqual(parseNumberAtIndex(test[0], test[1]), test[2]);
      });
    }
  });

  void describe('parseNumbers', () => {
    const tests: [string, number[]][] = [
      ['two1nine', [2, 1, 9]],
      ['eightwothree', [8, 2, 3]],
      ['abcone2threexyz', [1, 2, 3]],
      ['xtwone3four', [2, 1, 3, 4]],
      ['4nineeightseven2', [4, 9, 8, 7, 2]],
      ['zoneight234', [1, 8, 2, 3, 4]],
      ['7pqrstsixteen', [7, 6]],
      ['1sevenseven7ld', [1, 7, 7, 7]],
    ];

    for (const test of tests) {
      void it(`should parse ${test[1].join(', ')} numbers from string ${test[0]}`, () => {
        assert.deepStrictEqual(parseNumbers(test[0]), test[1]);
      });
    }
  });

  void describe('concatNumbers', () => {
    const tests: [number[], number][] = [
      [[2, 1, 9], 29],
      [[8, 2, 3], 83],
      [[1, 2, 3], 13],
      [[2, 1, 3, 4], 24],
      [[4, 9, 8, 7, 2], 42],
      [[1, 8, 2, 3, 4], 14],
      [[7, 6], 76],
      [[7], 77],
      [[], NaN],
    ];

    for (const test of tests) {
      void it(`should concat ${test[0].join(', ')} numbers to ${test[1]}`, () => {
        assert.deepStrictEqual(concatNumbers(test[0]), test[1]);
      });
    }
  });
});
