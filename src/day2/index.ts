import { createReadlineInterface } from '../utils.js';

interface ICube {
  red: number;
  green: number;
  blue: number;
}

interface IGame {
  idx: number;
  cubes: ICube[];
}

export function parseGame(line: string): IGame {
  const matchedGame = line.match(/Game (\d+): (.*)/);
  if (matchedGame?.length !== 3) {
    throw new Error(`Invalid game: ${line}`);
  }
  const [, idx, cubes] = matchedGame;

  const game: IGame = {
    idx: Number(idx),
    cubes: [],
  };

  const matchedCubes = [...cubes.matchAll(/ ?([a-z0-9 ,]+);?/g)];
  for (const matchedCube of matchedCubes) {
    if (matchedCube.length !== 2) {
      throw new Error(`Invalid cubes: ${cubes}`);
    }
    const [, cubeStr] = matchedCube;

    const cube: ICube = {
      red: 0,
      green: 0,
      blue: 0,
    };

    const matchedColors = [...cubeStr.matchAll(/(\d*) (green|red|blue)/g)];
    for (const matchedColor of matchedColors) {
      if (matchedColor.length !== 3) {
        throw new Error(`Invalid cube: ${cubeStr}`);
      }
      const [, count, color] = matchedColor;
      switch (color) {
        case 'red':
          cube.red = Number(count);
          break;
        case 'green':
          cube.green = Number(count);
          break;
        case 'blue':
          cube.blue = Number(count);
          break;
      }
    }

    game.cubes.push(cube);
  }

  return game;
}

export function isGamePossible(game: IGame, red: number, green: number, blue: number): boolean {
  for (const cube of game.cubes) {
    if (cube.red > red || cube.green > green || cube.blue > blue) {
      return false;
    }
  }
  return true;
}

export function getGamePower(game: IGame): number {
  const reds: number[] = [];
  const greens: number[] = [];
  const blues: number[] = [];

  for (const cube of game.cubes) {
    if (cube.red) {
      reds.push(cube.red);
    }
    if (cube.green) {
      greens.push(cube.green);
    }
    if (cube.blue) {
      blues.push(cube.blue);
    }
  }

  return Math.max(...reds) * Math.max(...greens) * Math.max(...blues);
}

export async function runPartOne(path: string) {
  const rl = createReadlineInterface(path);
  const possibleGames: IGame[] = [];

  for await (const line of rl) {
    const game = parseGame(line);
    if (isGamePossible(game, 12, 13, 14)) {
      possibleGames.push(game);
    }
  }

  const sum = possibleGames.reduce((acc: number, value: IGame) => acc + value.idx, 0);

  return sum;
}

export async function runPartTwo(path: string) {
  const rl = createReadlineInterface(path);
  const powers: number[] = [];

  for await (const line of rl) {
    const game = parseGame(line);
    const power = getGamePower(game);
    powers.push(power);
  }

  const sum = powers.reduce((acc: number, value: number) => acc + value, 0);

  return sum;
}
