import { basename } from 'path';
import { fileURLToPath } from 'url';
import Enquirer from 'enquirer';
import { glob } from 'glob';

interface IDataOption {
  label: string;
  path: string;
}

interface IDayOption {
  label: string;
  folder: string;
  data: IDataOption[];
}

interface ISelectedDay {
  folder: string;
  dataPath: string;
}

interface IDayModule {
  runPartOne: (path: string) => Promise<number>;
  runPartTwo: (path: string) => Promise<number>;
}

async function getDayOptions(): Promise<IDayOption[]> {
  const days: IDayOption[] = [];
  const cwd = fileURLToPath(new URL('.', import.meta.url));
  const folders = await glob('day*/', { cwd });

  for (const folder of folders) {
    const dataPaths = await glob(`${folder}/data/*.txt`, { cwd });
    days.push({
      label: `Day ${folder.slice(3)}`,
      folder,
      data: dataPaths
        .map((path) => ({ label: basename(path), path }))
        .sort((a, b) => a.label.localeCompare(b.label, 'en', { numeric: true })),
    });
  }

  return days.sort((a, b) => a.label.localeCompare(b.label, 'en', { numeric: true }));
}

async function selectDay(options: IDayOption[]): Promise<ISelectedDay> {
  const { day } = await Enquirer.prompt<{ day: string }>({
    type: 'autocomplete',
    name: 'day',
    message: 'Which day to run',
    choices: options.map((day) => ({ name: day.label, value: day.folder })),
  });
  const { data } = await Enquirer.prompt<{ data: string }>({
    type: 'autocomplete',
    name: 'data',
    message: 'Which data to use',
    choices: options.find((option) => option.folder === day)?.data.map((d) => ({ name: d.label, value: d.path })),
  });
  return { folder: day, dataPath: data };
}

async function runDay(day: ISelectedDay) {
  const module = (await import(new URL(day.folder, import.meta.url).href)) as IDayModule;
  if (typeof module.runPartOne === 'function' && typeof module.runPartTwo === 'function') {
    const path = fileURLToPath(new URL(day.dataPath, import.meta.url));

    const resPartOne = await module.runPartOne(path);
    console.log(`Part 1 result: ${resPartOne}`);

    const resPartTwo = await module.runPartTwo(path);
    console.log(`Part 2 result: ${resPartTwo}`);
  } else {
    throw new Error(`Invalid ${day.folder} module`);
  }
}

async function main() {
  const options = await getDayOptions();
  const day = await selectDay(options);
  await runDay(day);
}

main().catch((err) => {
  console.error(err);
  process.exit(1);
});
